fetch('./output/matchesPerYear.json')
  .then(response => response.json())
  .then(data =>{
      let matchesPerYear=[];
      let keys=Object.keys(data);
      for(let i in keys)
      {
        matchesPerYear.push([keys[i],data[keys[i]]]);
        
      }
    //   console.log(matchesPerYear);
    Highcharts.chart('matchesPerYear', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Matches Per Year'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
              
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No of Matches'
            }
        },
        legend: {
            enabled: false
        },
        // tooltip: {
        //     pointFormat: 'Population in 2017: <b>{point.y:.1f} millions</b>'
        // },
        series: [{
            name: 'Matches',
            data: matchesPerYear
            ,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
  });


  fetch('./output/extraRunsPerTeam2016.json')
  .then(response => response.json())
  .then(data =>{
    let extraRunPerTeam=[];
    let keys=Object.keys(data);
    for(let i in keys)
    {
      extraRunPerTeam.push([keys[i],data[keys[i]]]);
      
    }

    Highcharts.chart('extraRunPerTeam', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        title: {
            text: 'Extra run<br>\per Team<br>2016',
            align: 'center',
            verticalAlign: 'middle',
            y: 60
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.0f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%'],
                size: '110%'
            }
        },
        series: [{
            type: 'pie',
            name: 'Extra Runs',
            innerSize: '50%',
            data: extraRunPerTeam
        }]
    });
  });



    fetch('./output/top10EcoBowlers2015.json')
    .then(response => response.json())
    .then(data =>{

        let economySeries=[];
        for(let playerData in data)
        {
            // console.log(data[playerData][1])
            
            economySeries.push({name:data[playerData][0],low:data[playerData][1]});
        }
        // console.log(economySeries);

        Highcharts.chart('top10EcoBowler', {

            chart: {
                type: 'lollipop',
                inverted: true
            },
        
            accessibility: {
                point: {
                    valueDescriptionFormat: '{index}. {xDescription}, {point.y}.'
                }
            },
        
            legend: {
                enabled: false
            },
            title: {
                text: 'Top 10 Economy Bowlers'
            },
        
            tooltip: {
                shared: true
            },
        
            xAxis: {
                type: 'category'
            },
        
            yAxis: {
                title: {
                    text: 'Economy'
                }
            },
        
            series: [{
                name: 'Economy',
                data: economySeries
            }]
        
        });
    })



fetch("./output/wonTossAndMatch.json")
.then(response => response.json())
.then(data =>{
    
    let wonTeams=[];
    let matches=[]
    for(let teams in data){
        wonTeams.push(teams);
        matches.push(data[teams]);
    }
    
    
    Highcharts.chart("wonTossAndMatch", {
        chart: {
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 10,
                beta: 25,
                depth: 100
            }
        },
        title: {
            text: '<h1>Teams that won Toss and Match</h1>'
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        xAxis: {
            categories: wonTeams,
            labels: {
                skew3d: true,
                style: {
                    fontSize: '16px'
                }
            }
        },
        yAxis: {
            title: {
                text: "Matches"
            }
        },
        series: [{
            name: 'Won Toss and Match',
            data: matches
        }]
    });

});


fetch("./output/manOfTheMatch.json")
.then(response => response.json())
.then(data =>{

    let manOfTheMatch=[];
    for(let index in data)
    {
        manOfTheMatch.push({name:index,label:data[index]})
        // console.log(index)
    }
    // console.log(manOfTheMatch)
    Highcharts.chart('manOfTheMatch', {
        chart: {
            type: 'timeline'
        },
        xAxis: {
            visible: false
        },
        yAxis: {
            visible: false
        },
        title: {
            text: 'Highest number of Player of the Match awards for each season'
        },

        series: [{
            dataLabels: {
                connectorColor: 'silver',
                connectorWidth: 2
            },
            data: manOfTheMatch
        }]
        
    });


})


fetch("./output/matchesWonPerTeamPerYear.json")
.then(response => response.json())
.then(data =>{
    let matchesWonPerTeamPerYear=[];
    let teams=[];
    for(let index in data)
    {
        for(let team in data[index])
        {
            if(!teams.includes(team))
            {
                teams.push(team);
            }
        }
    }
    for(let team in teams)
    {
        let teamData=[];
        console.log(team);
        for(let season in data)
        {
            if(data[season][teams[team]]==undefined)
            {
                teamData.push(null);
            }
            else{
                teamData.push(data[season][teams[team]]);
            }
        }
        matchesWonPerTeamPerYear.push({name:teams[team],data:teamData})
    
    }

    Highcharts.chart('matchesWonPerTeamPerYear', {

        title: {
            text: 'Matches won per team per year in IPL'
        },

        yAxis: {
            title: {
                text: 'Matches won'
            }
        },
    
        xAxis: {
            accessibility: {
                rangeDescription: 'Range: 2008 to 2017'
            }
        },
    
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
    
        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: 2008
            }
        },
    
        series: matchesWonPerTeamPerYear,
    
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    
    });
})