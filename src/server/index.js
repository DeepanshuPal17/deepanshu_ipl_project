const csvToJson=require('csvtojson');
const path=require('path');
const ipl= require(path.resolve('./src/server/ipl'));
const matchesCsvFilePath=path.resolve('./src/data/matches.csv');
const deliveriesCsvFilePath=path.resolve('./src/data/deliveries.csv');
const fs=require('fs');

csvToJson()
.fromFile(matchesCsvFilePath)
.then((matchesData)=>{
    
    // Number of matches played per year for all the years in IPL
    const matchPerYear=ipl.matchesPerYear(matchesData);
    fs.writeFile(path.resolve('./src/public/output/matchesPerYear.json'),JSON.stringify(matchPerYear,"",2),(err)=>{
        if(err)
            console.log("Error accure in matchesPerYear ");
        console.log("File updated")
    });

    //Number of matches won per team per year in IPL
    const matchesWonPerTeam=ipl.matchesWonPerTeam(matchesData);
    fs.writeFile(path.resolve('./src/public/output/matchesWonPerTeamPerYear.json'),JSON.stringify(matchesWonPerTeam,"",2),(err)=>{
        if(err)
            console.log("Error accure in  matchesWonPerTeamPerYear");
        console.log("File updated");
    });

    //the number of times each team won the toss and also won the match
    const wonTossAndMatch=ipl.wonTossAndMatch(matchesData);
    fs.writeFile(path.resolve('./src/public/output/wonTossAndMatch.json'),JSON.stringify(wonTossAndMatch,"",2),(err)=>{
        if(err)
            console.log("Error accure in wonTossAndMatch ");
        console.log("File updated");
    });

    // a player who has won the highest number of Player of the Match awards for each season
    const manOfTheMatch=ipl.manOfTheMatch(matchesData);
    fs.writeFile(path.resolve('./src/public/output/manOfTheMatch.json'),JSON.stringify(manOfTheMatch,"",2),(err)=>{
        if(err)
            console.log("Error accure in manOfTheMatch ");
        console.log("File updated");
    });

    //Extra runs conceded per team in the year 2016
    csvToJson()
    .fromFile(deliveriesCsvFilePath)
    .then((deliveriesData)=>{
        
        const extraRunPerTeam2016=ipl.extraRunsPerTeam2016(matchesData,deliveriesData);
        fs.writeFile(path.resolve('./src/public/output/extraRunsPerTeam2016.json'),JSON.stringify(extraRunPerTeam2016,"",2),(err)=>{
            if(err)
                console.log("Error accure in extraRunsPerTeam2016");
            console.log("File updated");
        });

        //Top 10 economical bowlers in the year 2015
        const top10EcoBowlers2015=ipl.top10EcoBowlers2015(matchesData,deliveriesData);
        fs.writeFile(path.resolve('./src/public/output/top10EcoBowlers2015.json'),JSON.stringify(top10EcoBowlers2015,"",2),(err)=>{
            if(err)
                console.log("Error accure in top10EcoBowlers2015 ");
            console.log("File updated");
        });

        //bowler with the best economy in super overs
        const bowlerWithBestEconomy=ipl.bowlerWithBestEconomy(deliveriesData);
        fs.writeFile(path.resolve('./src/public/output/bowlerWithBestEconomy.json'),JSON.stringify(bowlerWithBestEconomy,"",2),(err)=>{
            if(err)
                console.log("Error accure in bowlerWithBestEconomy ");
            console.log("File updated");
        });

        //
        const strikeRateOfBatsman=ipl.strikeRateOfBatsman(matchesData, deliveriesData);
        fs.writeFile(path.resolve('./src/public/output/strikeRateOfBatsman.json'),JSON.stringify(strikeRateOfBatsman,"",2),(err)=>{
            if(err)
                console.log("Error accure in strikeRateOfBatsman");
            console.log("File updated");
        });
    });
});