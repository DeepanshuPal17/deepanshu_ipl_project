const bowlerWithBestEconomy = require('./SubIPL/bowlerWithBestEconomy');
const extraRunsPerTeam2016 = require('./SubIPL/extraRunsPerTeam2016');
const manOfTheMatch = require('./SubIPL/manOfTheMatch');
const matchesPerYear = require('./SubIPL/matchesPerYear');
const matchesWonPerTeam =require('./SubIPL/matchesWonPerTeam');
const strikeRateOfBatsman = require('./SubIPL/strikeRateOfBatsman');
const top10EcoBowlers2015 = require('./SubIPL/top10EcoBowlers2015');
const wonTossAndMatch=require('./SubIPL/wonTossAndMatch');

module.exports={
extraRunsPerTeam2016,
top10EcoBowlers2015,
matchesPerYear,
matchesWonPerTeam,
wonTossAndMatch,
manOfTheMatch,
bowlerWithBestEconomy,
strikeRateOfBatsman
}