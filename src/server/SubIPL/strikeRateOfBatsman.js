module.exports= function strikeRateOfBatsman(matchesData, deliveriesData)
{
    let batsmanData={};
    for(let match in matchesData)
    {
        if(batsmanData[matchesData[match].season]==undefined)
        {
            batsmanData[matchesData[match].season]={};
        }
        for(let deliveries in deliveriesData)
        {
            if(deliveriesData[deliveries].match_id===matchesData[match].id)
            {
                if(batsmanData[matchesData[match].season][deliveriesData[deliveries].batsman]===undefined)
                {
                    batsmanData[matchesData[match].season][deliveriesData[deliveries].batsman]={run:parseInt(deliveriesData[deliveries].batsman_runs),ball:1,strikeRate:deliveriesData[deliveries].batsman_runs};
                }
                else{
                    batsmanData[matchesData[match].season][deliveriesData[deliveries].batsman].run+=parseInt(deliveriesData[deliveries].batsman_runs);
                    batsmanData[matchesData[match].season][deliveriesData[deliveries].batsman].ball+=1;
                    batsmanData[matchesData[match].season][deliveriesData[deliveries].batsman].strikeRate=batsmanData[matchesData[match].season][deliveriesData[deliveries].batsman].run*100/ batsmanData[matchesData[match].season][deliveriesData[deliveries].batsman].ball;
                }
            }
        }
    }
    let strikeRateOfBatsman={};
    for(let season in batsmanData)
    {
        if(strikeRateOfBatsman[season]==undefined)
        {
            strikeRateOfBatsman[season]={};
        }
        for(let batsman in batsmanData[season])
        {
            strikeRateOfBatsman[season][batsman]=batsmanData[season][batsman].strikeRate;
        }
    }
    let batsmanStrike={}
    for(let season in strikeRateOfBatsman)
    {
        for(let player in strikeRateOfBatsman[season])
            {
                if(batsmanStrike[player]===undefined)
                {
                    batsmanStrike[player]={};
                    batsmanStrike[player][season]=parseFloat(strikeRateOfBatsman[season][player]).toFixed(2) ;
                }
                else{
                    batsmanStrike[player][season]=parseFloat(strikeRateOfBatsman[season][player]).toFixed(2) ;
                }
            }
    }
    // console.log(batsmanStrike);
    return batsmanStrike;
}