module.exports=function manOfTheMatch(matchesData)
{
    let players={};
    matchesData.reduce((acc,matchdata)=>{
        
        if(players[matchdata.season]===undefined)
        {players[matchdata.season]={}}
        if(players[matchdata.season][matchdata.player_of_match]===undefined)
        {
            players[matchdata.season][matchdata.player_of_match]=1;
        }
        else {
            players[matchdata.season][matchdata.player_of_match]+=1;
        }

        return players;
    })
    const higestManOfTheMatch={};
    for(const key in players){
        let seasonBest =Object.entries(players[key]).sort(([,a],[,b])=>b-a).slice(0,1);
        higestManOfTheMatch[key]=seasonBest[0][0];
        
    }
    // console.log(higestManOfTheMatch)
    return higestManOfTheMatch;
}