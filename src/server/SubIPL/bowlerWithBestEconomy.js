module.exports=function bowlerWithBestEconomy(deliveriesData)
{
    let bowlersEco={};

            for(let deliveries in deliveriesData)
            {   
                if(deliveriesData[deliveries]['is_super_over']==1)
                {
                    if(bowlersEco[deliveriesData[deliveries]['bowler']]===undefined)
                    {
                        // bowlersEco[]={}
                        bowlersEco[deliveriesData[deliveries]['bowler']]={run:parseInt(deliveriesData[deliveries]['wide_runs'])+parseInt(deliveriesData[deliveries]['noball_runs'])+parseInt(deliveriesData[deliveries]['penalty_runs'])+parseInt(deliveriesData[deliveries]['batsman_runs']),ball:1,eco:0};
                    }
                    else
                    {
                        bowlersEco[deliveriesData[deliveries]['bowler']]['run']+=parseInt(deliveriesData[deliveries]['wide_runs'])+parseInt(deliveriesData[deliveries]['noball_runs'])+parseInt(deliveriesData[deliveries]['penalty_runs'])+parseInt(deliveriesData[deliveries]['batsman_runs']);
                        bowlersEco[deliveriesData[deliveries]['bowler']]['ball']+=1;
                        bowlersEco[deliveriesData[deliveries]['bowler']]['eco']=(bowlersEco[deliveriesData[deliveries]['bowler']]['run']/bowlersEco[deliveriesData[deliveries]['bowler']]['ball'])*6;
                    }
                }
            }
    
    // console.log(bowlersEco);
    bowlersEco=Object.entries(bowlersEco).sort(([,a],[,b])=>a.eco-b.eco).slice(0,1);
    return bowlersEco
    .map((bowler)=>{
        let obj={}
        let name=bowler[0];
        return [name,bowler[1]['eco']];
    })
}