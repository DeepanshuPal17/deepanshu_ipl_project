module.exports=function extraRunPerTeam2016(matchesData,deliveriesData)
{
    let extraRun={};
    for(let match in matchesData)
    {
        if(matchesData[match]['season']==2016)
        {   
            for(let deliveries in deliveriesData)
            {   
                if(matchesData[match]['id']==deliveriesData[deliveries]['match_id'])
                {
                    if(extraRun[deliveriesData[deliveries]['bowling_team']]===undefined)
                    {
                        extraRun[deliveriesData[deliveries]['bowling_team']]=parseInt(deliveriesData[deliveries]['extra_runs']);
                    }
                    else
                    {
                        extraRun[deliveriesData[deliveries]['bowling_team']]+=parseInt(deliveriesData[deliveries]['extra_runs']);
                    }
                }
            }
        }
    }
    return extraRun;
}