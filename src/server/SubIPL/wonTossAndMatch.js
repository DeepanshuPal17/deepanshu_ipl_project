module.exports=function wonTossAndMatch(matchesData)
{
    let wonTosMatch={};
    matchesData.reduce((acc,matchdata)=>{
        if(matchdata.toss_winner===matchdata.winner)
        {
            if(wonTosMatch[matchdata.toss_winner]===undefined)
            {
                wonTosMatch[matchdata.toss_winner]=1;
            }
            else {
                wonTosMatch[matchdata.toss_winner]+=1;
            }
        }
        return wonTosMatch;
    })
    return wonTosMatch;
}