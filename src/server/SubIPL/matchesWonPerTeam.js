module.exports=function matchesWonPerTeam(matchesData)
{
    let matches={};
    
    for(let index=0;index< matchesData.length;index++)
    {
        
        let year=matchesData[index]['season'];
        let wonTeam=matchesData[index]['winner'];
        
        if(matches[year]==undefined)
        {
            matches[year]={};
            matches[year][String(wonTeam)]=1;
        }
        else {
            if(matches[year][String(wonTeam)]==undefined)
            {
                matches[year][String(wonTeam)]=1;
            }
            else
            {
                matches[year][String(wonTeam)]+=1;
            }
        }
    }   
    return matches;
}