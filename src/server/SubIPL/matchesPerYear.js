module.exports=function matchesPerYear(matchesData)
{
    let matches={};
    
    for(let index=0;index< matchesData.length;index++)
    {
        
        let year=matchesData[index]['season'];
        
        if(matches[year]==undefined)
        {
            matches[year]=1;
        }
        else {
            matches[year]+=1;
        }
    }   
    return matches;
}